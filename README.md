# 번들러(Bundler) _ 권준한
- bundle : ~을 마구 싸 보내다.

### 1. 번들러(Bundler)가 어떻게 나오게 되었는가?
- 하나의 웹 서비스에서 수십 수백개의 JS파일을들 다루다가 문제가 하나 둘 씩 생겨나는 문제들이 나오기 시작함

+ ex) 1. 중복된 이름으로 인한 에러
#### index.html
```
<head>
    <script src="./hello.js"></script>
    <script src="./world.js"></script>
</head>
<body>
    <h1>Hello, Webpack</h1>
    <div id="root"></div>
    <script type="module">
        document.querySelector("#root").innerHTML = word;
    </script>
</body>
```

#### hello.js
```
var word = "Hello";
```

#### world.js
```
var word = "World"l
```

> 다음과 같은 파일들과 그 안에 코드들이 있다고 가정했을때, hello.js 파일안에도 word 라는 변수에 "Hello"라는 값이 할당 되어있고, 똑같이 world.js 파일에도 word 변수와 "Hello" 라는 값이 할당 되어있다. 그래서 사용자는 어떤 word를 사용하는지 구별하기가 힘들다. 만약에 이렇게 js파일이 적으면 문제를 방지하는데에 큰 어려움이 없겠지만 혼자 개발하는것이 아닌 많은 개발자들이 작업을 했을 경우 셀수 없이 많은 코드들이 작성되기 때문에 문제를 방지하는데엔 거의 불가능에 가까움

+ ex) 2. 파일 전송 문제
- 어떤 사이트에 들어갔을 때, 사용자는 서버로부터 요청에 따른 파일들을 가져오게 된다. 예를들어 하나의 파일을 요청하는데 1초가 걸린다면 100개의 파일을 요청하는데는 100초가 걸리게 되는데다. 이 부분은 비용적인 부분에서 문제가 생기게 된다. 만약 하나의 파일을 사용하면 해결이 되겠지만, 유지보수 측면에서는 그렇지 못하게된다.

> 출처: https://blog.leehov.in/24

### 2. 번들러(Bundler)란..?
- 번들러는 js파일들 뿐만아니라, 어플리케이션에 필요한 모든 종류의 파일들을 모듈 단위로 나눠, 최소한의 묶음(번들)로 만들어냄.
- 웹 어플리케이션을 구성하는 수많은 자원들을 하나의 파일료 병합 및 앞축해주는 동작을 모듈 번들링이라고함

> 빌드 = 번들링 = 변환 다 같은의미!

### 3. 번들러를 사용하는 이유..?
- js파일들을 기능단위로 모듈화하고, 묶어서 관리가 가능함
    + 쉽게 말해 여러개의 파일들을 하나의 파일로 묵어주는 역할을 한다고 생각하면됨.
- 하나의 파일로 만들어 줄 때, 종속성을 알아서 확인해, 사용하지 않은 파일은 포함하지 않음. => 파일의 크기를 줄여 빠르게 로딩함..
- 번들러를 사용함으로써, 모듈별로 작성하고, 모듈간 혹은 외부 라이브러리의 의존성을 쉽게 관리 가능.
- ES6 버전 이상의 스크립트를 사용 가능

+ 그래서 lib를 생성해서 다른 프로젝트 파일에서도 사용할 수 있도록 하는 부분이 bundler?...//**********************확인차 여쭤보기..


### 번들러의 종류
1. Webpack : 모듈을 함수로 감싸고 로더와 모듈캐시를 구현하는 번들 생성
    - 런타임시 각 모듈 함수들이 평가되어 모듈 캐시를 지움
    - 모듈을 함수로 감싸는 방식은 안정적이다. 전역 스코프에 있던 변수를 지역으로 변경해 줌으로써 충돌없이 모듈화를 시킬수 있음.

2. Rollup : 각 모듈들을 함수로 감싸 평가하는 웹팩과는 달리 롤럽은 코드들을 동일한 수준으로 올림. 그 이후에 번들링을 진행하는데 한번에 하기때문에 속도는 웹팩보다는 빠르고 결과물도 더 가볍다.
    - 앱에서는 웹팩 라이브러리는 롤업.


# Vite 
    - 번들링 시, Rollup 기반의 다양한 빌드 커맨드 사용 가능. 높은 수준으로 최적화된 static 리소스를 배포할수 있게끔해, 미리 정의된 설정을 제공함.
    - 배포 시, @vitejs/plugin-legacy 플러그인 사용해야함.
    - build를 하게되면 dist폴더가 생기는데 이 파일들을 다른 프로젝트에 넣을시 적용이되는건지 확인해보기 //*******************************************



### 피드백
- 번들러의 목적은 lib에만 있는것이 아님 => node를 사용해서 서버를 띄우는 경우에도 결국에는 번들 js파일을 읽는것
- dist 폴더에 아웃풋 파일이 나오면 이를 npm이나 nexus와 같은 remote registry에 배포했을때, package.json 파일과 함께 배포가 됨.
- 이후 외부 프로젝트에서 이를 다운 받았을 때, dist 폴더와 package.json파일을 다운로드 받게되는 것이고, 해당 dist 폴더엔 어떤 정보가 있는지 package.json을 통해 인식함 (타입, entrypoint 등)
